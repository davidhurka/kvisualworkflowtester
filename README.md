<!--
SPDX-FileCopyrightText: 2021 David Hurka <david.hurka@mailbox.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# KVisualWorkflowTester

A KXmlGui application for automatic GUI testing.
This application is being created for the KDE Eco project.

Creating GUI usage scenarios shall be very easy with this tool,
so developers of applications will create such scenarios voluntarily.
These usage scenarios are primarily meant for measuring the energy consumption.

## Intended features

 * System under Test (SuT)
    - Interfaced through some mean of virtual desktop,
      for example VNC or Docker + Xephyr.
    - Interfaced with SSH at the same time,
      for e. g. setup of environment conditions before testing,
      and manipulation and verification via D-Bus.
 * Record GUI interactions with a SuT.
 * Replay GUI interactions on another SuT.
 * Import/export to other GUI testing/automation tools.
 * Usage scenarios based on a state transition system.
    - Different states are defined for the application.
    - Visual workflows are defined for transitions between two states,
      and for verification of a state.
    - Usage scenarios are defined as a list of states and transitions.
      This way, multiple alternative workflows can be part of a scenario.
    - SuT docker containers can be forked (`docker checkpoint`),
      to test different state transitions.
 * Visual verification with OpenCV.
 * Scripting with Python.
 * Easy version control for the scenario definition XML file.

## Differentiation to similar tools

### Actiona, SikuliX

 * Focused on testing a remote SuT (VNC)
 * Focused on exercising many different workflows instead of producing results (state transition system)
 * Recording feature

### Actiona

 * Visual verification

### SikuliX

 * More semantic workflow definition (XML instead of Python)

### Squish VNC Edition, Eggplant Functional

 * Free software :)
 * As easy to use as possible:
    - As many developers/maintainers as possible shall use it voluntarily.
    - Even interested users shall be invited to provide their favorite workflows.

## Mockups

### Scenario definition file

```xml
<scenarioDefinition xmlns="https://www.kde.org/standards/kvisualworkflowtester/scenarioDefinition"
                    xmlns:gui="https://www.kde.org/standards/kvisualworkflowtester/GUI"
                    xmlns:ssh="https://www.kde.org/standards/kvisualworkflowtester/SSH"
                    xmlns:python="https://www.kde.org/standards/kvisualworkflowtester/Python"
                    xmlns:openCV="https://www.kde.org/standards/kvisualworkflowtester/OpenCV"
                    xmlns:DBus="https://www.kde.org/standards/kvisualworkflowtester/D-Bus"
                    minimumVersion="0.0.3">
    <!--
    States represent application states that can be reached by some workflow.
    They shall be used for application states that allow or prevent some further workflow.
    They will often relate to a window being visible or some actions being enabled.

    For example, "printDialog" may be a state.
    This state can be reached by clicking a toolbar button or triggering a keyboard shortcut.
    In some cases, it can also be reached by clicking a "Print" button in the document.
    Notably, this state can *not* be reached from the "emptyShell" state.
    This state can be left by pressing "Print" or "Cancel", which goes back to the "documentOpen" state.
    This state can also be left by pressing "Properties", which opens the printer configuration dialog.

    The "documentOpen" state may be parametrized by the $document variable.
    The $document variable may have values like "Test1.pdf" or "20YearsOfKDE.pdf".
    These values should not represent states, because most workflows are independent of these values.

    It is not necessary to parametrize states, and there is no mechanism to assign variables to states.
    If parametrization is needed, you just assign values to variables.
    -->
    <states>
        <state name="emptyShell">
            <!--
            States can have verification workflows.
            These are assumed to not modify any application state, no matter how often they run.
            -->
            <verification>
                <!--
                The OpenCV plugin will have some actions that can make the test fail.

                The checkScreenshot action compares a screen region with a screenshot.
                A meta image can define areas in the screenshot, which are identified by meta colors.
                Meta colors can be hashed from identifiers like %windowTitle.
                -->
                <openCV:checkScreenshot region="mainWindow" image="empty_shell.png" metaImage="empty_shell.meta.png">
                    <!--
                    The checkScreenshot action fails if the %windowTitle area does not contain an isolated area of text that reads "Okular".
                    -->
                    <openCV:text metaColor="%windowTitle" matchType="localExact">Okular</openCV:text>
                    <!--
                    The checkScreenshot action fails if the %mainView area does not have similar features and colors to the reference screenshot.
                    -->
                    <openCV:area metaColor="%mainView" matchType="color features"/>
                </openCV:checkScreenshot>
            </verification>
        </state>
        <state name="documentOpen">
            <verification>
                <!--
                The D-Bus plugin will have actions that interface the SuT via D-Bus methods.
                -->
                <DBus:runMethod bus="session" wellKnownName="org.kde.okular*" path="/okular/org.kde.okular/currentDocument">
                    <!--
                    The first returned value needs to match some value exactly.
                    -->
                    <DBus:returnValue matchType="exact">
                        <!--
                        Variables are strings with global scope.
                        They can be accessed from Python as E.var["name"].
                        States are independent from variables, but variables can be used to parametrize them.
                        -->
                        <variable>$document</variable>
                    </DBus:returnValue>
                </DBus:runMethod>
            </verification>
        </state>
        <!--
        When KVisualWorkflowTester launches, it assumes to be in specialState:startup.
        To satisfy the verification, it is usually necessary to connect to a SuT.
        -->
        <state name="specialState:startup">
            <verification>
                <!--
                The SSH plugin will have actions to run shell commands on the SuT.
                This allows to initialize configuration files, install packages, etc.
                -->
                <ssh:runCommand>
                    <ssh:command>okular -v</ssh:command>
                    <ssh:output matchType="regex">^okular \d+\.\d+\.\d+$</ssh:output>
                </ssh:runCommand>
            </verification>
        </state>
    </states>
    <!--
    Workflows are a list of actions.
    You can build a collection of various workflows, and then assemble them to a scenario.
    -->
    <workflows>
        <!--
        Transitions are workflows that reach a state from another state.
        Transitions shall represent possible GUI interactions.
        -->
        <transition before="specialState:startup" after="emptyShell" name="launchFromAppMenu">
            <!--
            The GUI plugin will have actions that perform user input through simulated input devices.
            -->
            <gui:keySequence>Meta</gui:keySequence>
            <gui:type>okular</gui:type>
            <gui:keySequence>Return</gui:keySequence>
            <openCV:waitForRegion image="empty_shell.png" matchType="color features" storeRegion="mainWindow"/>
        </transition>
        <transition before="specialState:startup" after="emptyShell" priority="-1">
            <ssh:startCommand>okular</ssh:startCommand>
            <!--
            The waitForWindow action waits until something similar to a reference screenshot appears.
            -->
            <openCV:waitForWindow image="empty_shell.png" storeRegion="mainWindow"/>
        </transition>
        <transition before="emptyShell" after="documentLoaded">
            <!--
            The waitForWindow action can also wait for arbitrary features that appear in response to actions.
            -->
            <openCV:waitForWindow storeRegion="fileMenu">
                <!--
                The locateText action can locate text within a screen region.
                The position is passed to the click action.
                -->
                <gui:click><openCV:locateText region="mainWindow" image="empty_shell.png" metaImage="empty_shell.meta.png" metaColor="%menuBar" matchType="localExact">File</openCV:locateText></gui:click>
            </openCV:waitForWindow>
            <gui:click><openCV:locateText region="fileMenu" matchType="localExact wholeLine">Open...</openCV:locateText></gui:click>
            <openCV:waitForWindow image="file_dialog.png" storeRegion="fileDialog"/>
            <gui:click><openCV:locateText region="fileDialog" image="file_dialog.png" metaImage="file_dialog.meta.png" metaColor="%places" matchType="localExact wholeLine">Documents</openCV:locateText></gui:click>
            <gui:click><openCV:locateText region="fileDialog" image="file_dialog.png" metaImage="file_dialog.meta.png" metaColor="%mainView" matchType="localExact"><variable>$document</variable></openCV:locateText></gui:click>
        </transition>
        <!--
        Macros are workflows that do not change the state.
        Macros shall be used for abstraction (for simplified version control),
        and for the DRY (don't repeat yourself) idiom.
        -->
        <macro name="moveSplitter" comment="Moves the main window splitter (sidebar) to $splitterPosition (0..1).">
            <set variable="pointBefore"><openCV:getPosition region="mainWindow" image="empty_shell.png" metaImage="empty_shell.meta.png" metaColor="%splitter"/></set>
            <!--
            The Python plugin will have actions to run snippets of Python code.

            Other plugins and the KVisualWorkflowTester core can be accessed through the variable "E".
            -->
            <set variable="pointAfter"><python:expression>E.point(E.region["mainWindow"].left + E.region["mainWindow"].width * E.var["splitterPosition"], E.var["pointBefore"].y)</python:expression></set>
            <gui:drag durationMs="1000">
                <gui:dragPoint><variable>$pointBefore</variable></gui:point>
                <gui:dragPoint><variable>$pointAfter</variable></gui:point>
            </gui:drag>
        </macro>
    </workflows>
    <!--
    Scenarios are like workflows, except that they can use reachState and useScenario.
    -->
    <scenarios>
        <scenario name="standardUsageScenario">
            <reachState name="emptyShell"/>
        </scenario>
        <scenario name="energyMeasurement">
            <useScenario name="standardUsageScenario"/>
            <set variable="splitterPosition">0.25</set>
            <useMacro name="moveSplitter"/>
        </scenario>
    </scenarios>
</scenarioDefinition>
```

## Roadmap

 * [ ] Copy VNC widget from KRdc
 * [ ] Menu entry to connect to a VNC server
 * [ ] Control SuT through the VNC widget
 * [ ] Emit signals from the VNC widget when some user interaction happens
 * [ ] Add slots to the VNC client to provide user interaction
 * [ ] Scenario definition object
 * [ ] GUI plugin records VNC widget signals
 * [ ] GUI plugin adds actions to a default scenario
 * [ ] Default scenario can be played to the VNC client
 * [ ] Make a screencast to demonstrate the functionality
 * [ ] Add remaining features to the scenario definition object
    - [ ] States
    - [ ] Transitions
    - [ ] Macros
    - [ ] Multiple scenarios
 * [ ] Add user interface for states, transitions, macros, scenarios
 * [ ] Variables
 * [ ] Plugins
    - [ ] SSH
    - [ ] Python
    - [ ] D-Bus
    - [ ] OpenCV
 * [ ] Make VNC interface a plugin
