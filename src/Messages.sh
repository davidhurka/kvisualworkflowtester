#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2021 David Hurka <david.hurka@mailbox.org>
#
# SPDX-License-Identifier: BSD-2-Clause

$EXTRACTRC `find . -name \*.rc` >> rc.cpp
$XGETTEXT `find . -name \*.cpp` -o $podir/kvisualworkflowtester.pot
