// SPDX-FileCopyrightText: 2021 David Hurka <david.hurka@mailbox.org>
//
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#include "mainwindow.h"

// TODO This is a mess, I think I need only 15% of these includes.
#include <KParts/ReadWritePart>
#include <KPluginLoader>
#include <KPluginFactory>
#include <KPluginMetaData>
#include <KActionCollection>
#include <KStandardAction>
#include <KLocalizedString>
#include <KMessageBox>

#include <QApplication>
#include <QFileDialog>

namespace KVisualWorkflowTester
{

MainWindow::MainWindow()
    : KXmlGuiWindow()
{
    // set KXMLUI resource file
    setXMLFile(QStringLiteral("kvisualworkflowtesterui.rc"));

    // setup our actions
    setupActions();

//     setCentralWidget(m_part->widget());

    setupGUI();
}

MainWindow::~MainWindow()
{
}

void MainWindow::loadDocument(const QUrl& url)
{
}

void MainWindow::setupActions()
{
    KStandardAction::openNew(this, &MainWindow::fileNew, actionCollection());
    KStandardAction::open(this, &MainWindow::fileOpen, actionCollection());

    KStandardAction::quit(qApp, &QApplication::closeAllWindows, actionCollection());
}

void MainWindow::fileNew()
{
}

void MainWindow::fileOpen()
{
    const QUrl url = QFileDialog::getOpenFileUrl(this);
}

} // namespace KVisualWorkflowTester
