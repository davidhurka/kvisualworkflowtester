<!--
SPDX-FileCopyrightText: 2021 David Hurka <david.hurka@mailbox.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# KVisualWorkflowTester source code architecture overview

This section describes the design which I imagine for KVisualWorkflowTester.
It is a prediction and probably should be reviewed by an experienced software designer.

## Logic core

The logic core is done with a QObject which has a lot of signals and slots.
Most slots are forwarded to a signal, which is then forwarded to all plugins.
Signals use complex parameters, which are usually events that shall be accepted by one plugin.

The core provides the scenario description object and other central structures.
Otherwise, the core has only simple internal states.
When it receives user input, it may forward it to all plugins or only to the SuT.

## Plugins

Various plugins are connected to the logic core via signals and slots.
They may respond to signals e. g. by creating a widget through which the user can control the SuT.
The new widget will send signals when the user does input to the SuT.

Another plugin can receive the user input signals.
They can accept them to show that they make use of them, and send a new signal with a workflow action.
They may also offer to modify the current workflow by sending signals.

### Services and widgets

There are two important classes of plugins, which are specific to the nature of the SuT:
Services are interfaces to the SuT, and widgets are appropriate interfaces to the user.

Usually, a service provides a graphical QWidget, a TTY stream, a D-Bus interface, etc.
A widget displays a graphical representation of the service.
If the user does input on this graphical representation, the widget sends a UserInputEvent.
The UserInputEvent is forwarded to the service, at the same time it can be converted to a workflow item.

Services and widgets need to be specific to each other.
For example, a virtual TTY widget can not provide a graphical representation of a VNC service.
But a widget can theoretically represent different services.
A virtual TTY widget may represent both an SSH session and an interactive session of a Python plugin.

A service can be used without a widget.
This may be done when a workflow shall be executed without supervision by a user.

A widget can theoretically be used without a service.
This may be done to create workflows without immediate feedback from a sample SuT.

Services and widgets may be nested.
For example, when a VNC service is created, it may request to create an SSH service to use it as tunnel.
A text recognition plugin may create an overlay widget in the virtual screen widget,
with which the user can select screen regions.

Widgets do not necessarily need a service.
For example, a widget may provide a graphical overview of available workflows.

### Workers

There is a third class of plugins.
For example, they convert between WorkflowItem and UserInputEvent.
They also watch out for possible simplifications on the workflow and offer changes.
They can also run Python scripts (inside the plugin, not on the SuT).

A worker plugin can record all performed workflow items and their translation to UserInputEvents.
This recording can then be used to export the workflow to simple xdotool or Xnee scripts.

A worker may usually be implemented in the PluginObject of the plugin.

## Interface mockup

### KVisualWorkflowTester::LogicCore

 * slots
    - slotRecordedUserInput(UserInputEvent) (from widgets)
    - slotRecordedSutOutput(SutOutputEvent)
    - slotPerformUserInput(UserInputEvent)
    - slotAppendWorkflowItem(WorkflowItem) (from services)
    - slotOfferWorkflowModification(WorkflowModification)
    - slotApplyWorkflowModification(WorkflowModification)
 * signals
    - signalRecordedUserInput(UserInputEvent)
    - signalPerformUserInput(UserInputEvent)
    - signalRecordedUserInput(UserInputEvent)
    - signalPerformWorkflowItem(WorkflowItem)
    - signalPerformedWorkflowItem(WorkflowItem, result)
    - signalSelectedWorkflowModification(WorkflowModification)
    - signalWorkflowModified(WorkflowModification)
    - signalCurrentWorkflowItemChanged(WorkflowItem)
 * Plugin management signals
    - signalRequestService(ServiceRequest)
    - signalServiceCreated(Service)
    - signalRequestWidget(WidgetRequest)
    - signalWidgetCreated(Widget)
 * Plugin management slots
    - slotServiceCreated(Service) (For services which create themselves spontaneously)
    - slotWidgetCreated(Widget) (For widgets which create themselves spontaneously)

### KvwtVncPlugin::PluginObject

 * slots
    - slotRequestService(ServiceRequest)

### KvwtVncPlugin::VncSession

 * slots
    - slotPerformUserInput(UserInputEvent)
 * signals
    - signalRecordedSutOutput(SutOutputEvent) (For meta stuff, like broken connections)

### KvwtVirtualScreenPlugin::PluginObject

 * slots
    - slotRequestWidget(WidgetRequest)

### KvwtVirtualScreenPlugin::VirtualScreen

 * slots
    - slotRequestWidget(WidgetRequest) (For screen overlays; like OpenCV region selection, gui:drag path drawing)
 * signals
    - signalRecordedUserInput(UserInputEvent) (To LogicCore -> performUserInput; or fast-forward to VncSession)

### KvwtGuiPlugin::PluginObject

 * slots
    - slotWidgetCreated(Widget) (To create screen overlays on each VirtualScreen widget)
    - slotRecordedUserInput(UserInputEvent) (-> appendWorkflowItem)
    - slotWorkflowModified(WorkflowModification) (-> offerWorkflowModification)
 * signals
    - signalWidgetCreated(Widget) (To advertise the VirtualScreen overlay widget)
    - signalAppendWorkflowItem(WorkflowItem)
    - signalOfferWorkflowModification(WorkflowModification)

## Example call flows

### Create a virtual screen with VNC connection to the SuT

Workflow action: create a VNC connection to the SuT in order to go from startup to desktop state.

 * LogicCore::signalRequestService()
 * KvwtVncPlugin::PluginObject::slotRequestService()
    - The VNC plugin creates a service and accepts the request.
 * LogicCore::signalServiceCreated()
 * KvwtGuiPlugin::PluginObject::slotServiceCreated()
    - The GUI plugin notices that a VNC session has been opened and requests a virtual screen.
      (In unattended mode, the GUI plugin would not request a widget.)
 * LogicCore::signalRequestWidget()
 * KvwtVirtualScreenPlugin::PluginObject::slotRequestWidget()
    - The virtual screen plugin creates a widget and accepts the request.
 * LogicCore::signalWidgetCreated()
 * KvwtGuiPlugin::PluginObject::slotWidgetCreated()
    - The GUI plugin spontaneously creates an overlay widget on the virtual screen.
 * KvwtGuiPlugin::PluginObject::signalWidgetCreated()
 * LogicCore::slotWidgetCreated()

### Create a mouse click workflow item

Real user input: click on the virtual screen widget while in recording mode.

 * KvwtVirtualScreenPlugin::VirtualScreen::signalRecordedUserInput()
 * LogicCore::slotRecordedUserInput()
 * LogicCore::signalRecordedUserInput()
 * KvwtGuiPlugin::PluginObject::slotRecordedUserInput()
    - The GUI plugin creates a gui:click workflow item with an absolute screen coordinate.
 * KvwtGuiPlugin::PluginObject::signalAppendWorkflowItem()
 * LogicCore::slotAppendWorkflowItem()
 * LogicCore::signalWorkflowModified()
 * KvwtOpencvPlugin::ScreenOverlay::slotWorkflowModified()
    - The OpenCV plugin thinks the absolute coordinate could be replaced by a region on a reference screenshot.
 * KvwtOpencvPlugin::ScreenOverlay::signalOfferWorkflowModification()
 * LogicCore::slotOfferWorkflowModification()
    - The user can now select this workflow modification.
      Since there is the possibility to fine tune this modification, the OpenCV widget needs to be shown.
 * LogicCore::signalSelectedWorkflowModification()
 * KvwtOpencvPlugin::ScreenOverlay::slotSelectedWorkflowModification()
    - When the user has fine tuned the modification, it can be applied from the specific widget.
 * KvwtOpencvPlugin::ScreenOverlay::signalApplyWorkflowModification()
 * LogicCore::slotApplyWorkflowModification()
 * LogicCore::signalWorkflowModified()

The user input also needs to be forwarded to KvwtVncPlugin::VncSession::slotPerformUserInput(),
if not disabled by the virtual screen widget.

### Execute a workflow item

The workflow is being replayed and a gui:click workflow item has been reached.

 * LogicCore::signalPerformWorkflowItem()
 * KvwtGuiPlugin::PluginObject::slotPerformWorkflowItem()
 * KvwtGuiPlugin::PluginObject::signalPerformUserInput()
 * LogicCore::slotPerformUserInput()
 * LogicCore::signalPerformUserInput()
 * KvwtVncPlugin::VncSession::slotPerformUserInput()
 * LogicCore::signalPerformedWorkflowItem()

To enable signalPerformedWorkflowItem(), the WorkflowItem needs some attached object that can record being used.
