// SPDX-FileCopyrightText: 2021 David Hurka <david.hurka@mailbox.org>
//
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#include "mainwindow.h"

#include <KAboutData>
#include <KLocalizedString>

#include <QApplication>
#include <QCommandLineParser>
#include <QUrl>
#include <QDir>
#include <QIcon>

#include "version.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    KLocalizedString::setApplicationDomain("kvisualworkflowtester");

    KAboutData aboutData(QStringLiteral("kvisualworkflowtester"),
        i18nc("@title About dialog", "KVisualWorkflowTester"), // TODO
        QStringLiteral(KVISUALWORKFLOWTESTER_VERSION_STRING),
        i18nc("@info About dialog", "A GUI testing and workload generation tool, created for KDE Eco."),
        KAboutLicense::GPL,
        i18nc("@info About dialog", "(C) 2021 David Hurka"));
    aboutData.addAuthor(i18nc("@info About dialog: authors", "David Hurka"), i18nc("@info About dialog: authors", "Maintainer"), QStringLiteral("david.hurka@mailbox.org"));

    KAboutData::setApplicationData(aboutData);
    app.setWindowIcon(QIcon::fromTheme(QStringLiteral("kvisualworkflowtester")));

    QCommandLineParser parser;
    aboutData.setupCommandLine(&parser);
    parser.addPositionalArgument(QStringLiteral("urls"), i18n("Scenario file(s) to load."), QStringLiteral("[urls...]"));

    parser.process(app);
    aboutData.processCommandLine(&parser);

    const QStringList urls = parser.positionalArguments(); // TODO Process this.

    if (urls.isEmpty()) {
        auto window = new KVisualWorkflowTester::MainWindow;
        window->show();
    } else {
        for (const auto &url : urls) {
            auto window = new KVisualWorkflowTester::MainWindow;
            window->show();
            window->loadDocument(QUrl::fromUserInput(url, QDir::currentPath(), QUrl::AssumeLocalFile));
        }
    }

    return app.exec();
}
