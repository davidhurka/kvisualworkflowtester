// SPDX-FileCopyrightText: 2021 David Hurka <david.hurka@mailbox.org>
//
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <KXmlGuiWindow>

namespace KVisualWorkflowTester
{

/**
 * @short kvisualworkflowtester Shell
 */
class MainWindow : public KXmlGuiWindow
{
    Q_OBJECT
public:
    MainWindow();
    ~MainWindow() override;

    /**
     * Use this method to load whatever file/URL you have
     * @param url document to load
     */
    void loadDocument(const QUrl& url);

private Q_SLOTS:
    void fileNew();
    void fileOpen();

private:
    void setupActions();
};

} // namespace KVisualWorkflowTester

#endif // MAINWINDOW_H
